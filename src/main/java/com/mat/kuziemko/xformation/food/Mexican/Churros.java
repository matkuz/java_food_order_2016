package com.mat.kuziemko.xformation.food.Mexican;


import com.mat.kuziemko.xformation.food.DessertDecorate;
import com.mat.kuziemko.xformation.food.Lunch;

public class Churros  extends MexicanFood implements DessertDecorate {

    private Lunch course;

    public Churros(Lunch course){
        super();
        this.course = course;
    }

    public float getPrice() {
        return course.getPrice() + 10;
    }
    public String getName() {
        return course.getName() + ", Churros";
    }
}
