package com.mat.kuziemko.xformation.food.Drinks;

import com.mat.kuziemko.xformation.food.DrinkDecorate;
import com.mat.kuziemko.xformation.food.DrinkInterface;

public class Lemon implements DrinkDecorate {

    private DrinkInterface drink;

    public Lemon(DrinkInterface drink) {
        super();
        this.drink = drink;
    }

    public float getPrice() {
        return drink.getPrice() + 1;
    }

    public String getName() {
        return drink.getName() + ", Lemon";
    }
}
