package com.mat.kuziemko.xformation.food.Italian;

import com.mat.kuziemko.xformation.food.Lunch;

public class Pizza extends ItalianFood implements Lunch {
    public Pizza(){
        name = "Pizza";
    }
    public float getPrice() {
        return 5;
    }
}