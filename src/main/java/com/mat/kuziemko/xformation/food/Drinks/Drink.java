package com.mat.kuziemko.xformation.food.Drinks;
import com.mat.kuziemko.xformation.food.DrinkInterface;

public abstract class Drink implements DrinkInterface{
    protected String name;
    public String getName() {
        return name;
    }
}
