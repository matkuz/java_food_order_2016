package com.mat.kuziemko.xformation.food.Polish;

import com.mat.kuziemko.xformation.food.Lunch;

public class Golabki extends PolishFood implements Lunch {
    public Golabki(){
        name = "Gołabki";
    }
    public float getPrice() {
        return 10;
    }
}
