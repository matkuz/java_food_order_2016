package com.mat.kuziemko.xformation.food.Mexican;

import com.mat.kuziemko.xformation.food.Lunch;

public class Chilaquiles extends MexicanFood implements Lunch {
    public Chilaquiles(){
        name = "Chilaquiles";
    }
    public float getPrice() {
        return 2;
    }
}
