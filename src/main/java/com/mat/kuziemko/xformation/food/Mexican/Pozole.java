package com.mat.kuziemko.xformation.food.Mexican;


import com.mat.kuziemko.xformation.food.Lunch;

public class Pozole  extends MexicanFood implements Lunch {
    public Pozole(){
        name = "Pozole";
    }
    public float getPrice() {
        return 3;
    }
}
