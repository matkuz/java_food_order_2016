package com.mat.kuziemko.xformation.food;

import com.mat.kuziemko.xformation.food.Drinks.Drink;
import com.mat.kuziemko.xformation.food.Italian.ItalianFood;
import com.mat.kuziemko.xformation.food.Mexican.MexicanFood;
import com.mat.kuziemko.xformation.food.Polish.PolishFood;
import javassist.Modifier;
import org.reflections.Reflections;

import java.lang.reflect.*;
import java.util.*;

public class Menu {
    Reflections reflections;
    List<String> orders = new ArrayList<>();
    List<String> decorate = new ArrayList<>();

    public Menu(Reflections reflections) {
        this.reflections = reflections;
    }

    public int getCuisines() {
        Set<Class<? extends Food>> subTypes = reflections.getSubTypesOf(Food.class);
        int i = 1;
        for (Class<? extends Food> clazz : subTypes) {
            if (Modifier.isAbstract(clazz.getModifiers()) && !DrinkInterface.class.isAssignableFrom(clazz) && !clazz.isInterface()) {
                System.out.println(i + ". " + clazz.getSimpleName()); // OK!
                i++;
            }
        }
        ;
        return i;
    }

    public void getPolishFood() {
        int i = 1;
        Set<Class<? extends PolishFood>> subTypes = reflections.getSubTypesOf(PolishFood.class);
        for (Class<? extends PolishFood> clazz : subTypes) {
            if (!DessertDecorate.class.isAssignableFrom(clazz)) {
                System.out.println(i + ". " + clazz.getSimpleName());
                i++;
                orders.add(clazz.getName());
            }
        }
    }

    public void getPolishDesert() {
        int i = 1;
        Set<Class<? extends PolishFood>> subTypes = reflections.getSubTypesOf(PolishFood.class);
        for (Class<? extends PolishFood> clazz : subTypes) {
            if (DessertDecorate.class.isAssignableFrom(clazz)) {
                System.out.println(i + ". " + clazz.getSimpleName());
                i++;
                decorate.add(clazz.getName());
            }
        }
    }

    public void getMexicanDesert() {
        int i = 1;
        Set<Class<? extends MexicanFood>> subTypes = reflections.getSubTypesOf(MexicanFood.class);
        for (Class<? extends MexicanFood> clazz : subTypes) {
            if (DessertDecorate.class.isAssignableFrom(clazz)) {
                System.out.println(i + ". " + clazz.getSimpleName());
                i++;
                decorate.add(clazz.getName());
            }
        }
    }

    public void getMexicanFood() {
        int i = 1;
        Set<Class<? extends MexicanFood>> subTypes = reflections.getSubTypesOf(MexicanFood.class);
        for (Class<? extends MexicanFood> clazz : subTypes) {
            if (!DessertDecorate.class.isAssignableFrom(clazz)) {
                System.out.println(i + ". " + clazz.getSimpleName());
                i++;
                orders.add(clazz.getName());
            }
        }
    }

    public void getItalianDesert() {
        int i = 1;
        Set<Class<? extends ItalianFood>> subTypes = reflections.getSubTypesOf(ItalianFood.class);
        for (Class<? extends ItalianFood> clazz : subTypes) {
            if (DessertDecorate.class.isAssignableFrom(clazz)) {
                System.out.println(i + ". " + clazz.getSimpleName());
                i++;
                decorate.add(clazz.getName());
            }
        }
    }

    public void getItalianFood() {
        int i = 1;
        Set<Class<? extends ItalianFood>> subTypes = reflections.getSubTypesOf(ItalianFood.class);
        for (Class<? extends ItalianFood> clazz : subTypes) {
            if (!DessertDecorate.class.isAssignableFrom(clazz)) {
                System.out.println(i + ". " + clazz.getSimpleName());
                i++;
                orders.add(clazz.getName());
            }
        }
    }

    public void getDrinks() {
        int i = 1;
        Set<Class<? extends Drink>> subTypes = reflections.getSubTypesOf(Drink.class);
        for (Class<? extends Drink> clazz : subTypes) {
            if (!DrinkDecorate.class.isAssignableFrom(clazz)) {
                System.out.println(i + ". " + clazz.getSimpleName());
                i++;
                orders.add(clazz.getName());
            }
        }
    }

    public void getDrinksDecorate() {
        int i = 1;
        Set<Class<? extends DrinkDecorate>> subTypes = reflections.getSubTypesOf(DrinkDecorate.class);
        for (Class<? extends DrinkDecorate> clazz : subTypes) {
            System.out.println(i + ". " + clazz.getSimpleName());
            i++;
            decorate.add(clazz.getName());
        }
    }

    public void createOrder(int i, int j) {
        try {
            Food o = (Food) Class.forName(orders.get(i - 1)).newInstance();

            Class myClass = Class.forName(decorate.get(j - 1));
            Class[] argTypes = {Lunch.class};
            Constructor constructor = myClass.getDeclaredConstructor(argTypes);
            Object[] arguments = {o};
            Object instance = constructor.newInstance(arguments);

            Method printMethod = myClass.getMethod("getName", null);
            Method printPrice = myClass.getMethod("getPrice", null);

            System.out.println("-------------Your Orders:-------------");
            System.out.println(printMethod.invoke(instance, null).toString());
            System.out.println("To pay: "+printPrice.invoke(instance, null).toString());


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void createOrder2(int i, int j) {
        try {
            DrinkInterface o = (DrinkInterface) Class.forName(orders.get(i - 1)).newInstance();

            Class myClass = Class.forName(decorate.get(j - 1));
            Class[] argTypes = {DrinkInterface.class};
            Constructor constructor = myClass.getDeclaredConstructor(argTypes);
            Object[] arguments = {o};
            Object instance = constructor.newInstance(arguments);

            Method printMethod = myClass.getMethod("getName", null);
            Method printPrice = myClass.getMethod("getPrice", null);

            System.out.println("-------------Your Orders:-------------");
            System.out.println(printMethod.invoke(instance, null).toString());
            System.out.println("To pay: "+printPrice.invoke(instance, null).toString());


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public int getNumber(String text) {
        int number = 0;
        System.out.print(text);
        Scanner input = new Scanner(System.in);
        try {
            number = input.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Entered wrong number. Please, try again.");
            System.exit(1);
        }
        return number;
    }

    public void toEnd() {
        System.out.println("Thank you for your order! \nWe hope We can see you soon!");
    }
}
