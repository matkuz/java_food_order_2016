package com.mat.kuziemko.xformation.food.Drinks;

public class Sprite extends Drink {
    public Sprite() {
        name = "Sprite";
    }

    public float getPrice() {
        return 3;
    }
}
