package com.mat.kuziemko.xformation.food.Italian;

import com.mat.kuziemko.xformation.food.DessertDecorate;
import com.mat.kuziemko.xformation.food.Lunch;

/**
 * Created by Kuziemko-elo on 2016-06-12.
 */
public class Tiramisu extends ItalianFood implements DessertDecorate {

    private Lunch course;

    public Tiramisu(Lunch course) {
        super();
        this.course = course;
    }

    public float getPrice() {
        return course.getPrice() + 10;
    }

    public String getName() {
        return course.getName() + ", Tiramisu";
    }
}
