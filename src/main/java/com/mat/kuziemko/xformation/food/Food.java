package com.mat.kuziemko.xformation.food;

public interface Food {
    public float getPrice();
    public String getName();
}
