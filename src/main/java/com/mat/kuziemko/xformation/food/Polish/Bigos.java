package com.mat.kuziemko.xformation.food.Polish;

import com.mat.kuziemko.xformation.food.Lunch;


public class Bigos extends PolishFood implements Lunch {
    public Bigos(){
        name = "Bigos";
    }
    public float getPrice() {
        return 5;
    }
}