package com.mat.kuziemko.xformation.food.Polish;

import com.mat.kuziemko.xformation.food.DessertDecorate;
import com.mat.kuziemko.xformation.food.Lunch;

public class CreamCake   extends PolishFood implements DessertDecorate {

    private Lunch course;

    public CreamCake(Lunch course){
        super();
        this.course = course;
    }

    public float getPrice() {
        return course.getPrice() + 10;
    }
    public String getName() {
        return course.getName() + ", Creamcake";
    }
}
