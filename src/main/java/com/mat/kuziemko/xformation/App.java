package com.mat.kuziemko.xformation;

import com.mat.kuziemko.xformation.food.*;
import com.mat.kuziemko.xformation.food.Drinks.Drink;
import com.mat.kuziemko.xformation.food.Italian.*;
import com.mat.kuziemko.xformation.food.Polish.*;
import com.mat.kuziemko.xformation.food.Mexican.*;


import com.sun.org.apache.xpath.internal.operations.Mod;
import javassist.Modifier;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;
import java.util.Set;

/**
 * Main App
 *
 */
public class App
{
    public static void main( String[] args )
    {
        Reflections reflections = new Reflections("com.mat.kuziemko.xformation");


        Menu menu = new Menu(reflections);
        System.out.println("Hello Client !");

        System.out.println("Are you:");
        System.out.println("1.Hungry or");
        System.out.println("2.Tasty?");

        Integer choice = menu.getNumber("");
        if(choice.equals(1)) {

            menu.getCuisines();
            int orderNumber = menu.getNumber("Insert your choise:");

            switch (orderNumber) {
                case 1:
                    menu.getPolishFood();
                    break;
                case 2:
                    menu.getMexicanFood();
                    break;
                case 3:
                    menu.getItalianFood();
                    break;
                default:
                    System.out.print("Entered wrong number. Please, try again.");
                    break;
            }
            int orderNumber2 = menu.getNumber("Excelent choise. Enter your dishe: ");
            switch (orderNumber) {
                case 1:
                    menu.getPolishDesert();
                    break;
                case 2:
                    menu.getMexicanDesert();
                    break;
                case 3:
                    menu.getItalianDesert();
                    break;
                default:
                    System.out.print("Entered wrong number. Please, try again.");
                    break;
            }
            int orderNumber3 = menu.getNumber("For dessert?");
            menu.createOrder(orderNumber2, orderNumber3);
        }
        else if(choice.equals(2) ){
            menu.getDrinks();
            int orderNumber2 = menu.getNumber("What kind of drink do you want to drink? ");
            menu.getDrinksDecorate();
            int orderNumber3 = menu.getNumber("Do you want some additions?");
            menu.createOrder2(orderNumber2, orderNumber3);
        }
        else{
            System.out.println("Entered wrong number. Please, try again.");
            System.exit(1);
        }
        menu.toEnd();

    }
}